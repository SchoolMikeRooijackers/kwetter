/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import service.KwetterService;

@ServerEndpoint(
        value = "/ws/api"
)
public class KwetterEndpoint {

    /**
     * All open WebSocket sessions
     */
    static Map<String, ArrayList<Session>> peers = new HashMap<>();

    private Session wsSession;

    @Inject
    KwetterService kwetterService;

    @OnOpen
    public void openConnection(Session session) {
        if (session.getUserPrincipal() == null) {
            return;
        }
        addValues(session.getUserPrincipal().getName(), session);

        this.wsSession = session;

        send("New session started "
                + this.wsSession.getId()
                + " by user with username: "
                + session.getUserPrincipal().getName());
    }

    @OnClose
    public void closedConnection(Session session) {
        /* Remove this connection from the queue */
        for (String key : peers.keySet()) {
            for (Session s : peers.get(key)) {
                if (s == session) {
                    peers.get(key).remove(session);
                }
            }
        }
    }

    public static void send(String msg) {
        try {
            /* Send updates to all open WebSocket sessions for this match */
            for (String key : peers.keySet()) {
                for (Session session : peers.get(key)) {
                    session.getBasicRemote().sendObject(msg);
                }
            }
        } catch (EncodeException | IOException e) {
            e.printStackTrace();
        }
    }

    private void addValues(String key, Session value) {
        ArrayList tempList;
        if (peers.containsKey(key)) {
            tempList = peers.get(key);
            if (tempList == null) {
                tempList = new ArrayList();
            }
            tempList.add(value);
        } else {
            tempList = new ArrayList();
            tempList.add(value);
        }
        peers.put(key, tempList);
    }
}