/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Role implements Serializable {
    @Id
    @XmlElement(required = true)
    public String roleID;
    
    public Role(){}
    
    public Role(String roleID){
        this.roleID = roleID;
    }
    
    public String getGroupId(){
        return roleID;
    }
}
