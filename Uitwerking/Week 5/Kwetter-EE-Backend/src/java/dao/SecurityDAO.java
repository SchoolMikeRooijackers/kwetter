/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import domain.Role;
import domain.User;

/**
 *
 * @author Daan
 */
public interface SecurityDAO {

    void addRole(Role role);

    void addUserRole(User user, Role role);

    Role getRole(String name);
}
