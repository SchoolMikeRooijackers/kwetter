package service;

import dao.SecurityDAO;
import dao.SecurityDOA_JPAQualifier;
import java.util.List;
import javax.ejb.Stateless;
import dao.UserDAO;
import domain.Role;
import domain.User;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Inject;

@Stateless
@ApplicationScoped
public class KwetterService {

    @Inject
    private UserDAO userDAO;
    
    @Inject
    @SecurityDOA_JPAQualifier
    private SecurityDAO securityDAO;

    public KwetterService() {
    }

    public void create(User user) {
        userDAO.create(user);
    }

    public void edit(User user) {
        userDAO.edit(user);
    }

    public void remove(User user) {
        userDAO.remove(user);
    }

    public List<User> findAll() {
        return userDAO.findAll();
    }

    public User find(Object id) {
        return userDAO.find((Long) id);
    }

    public User find(String name) {
        return userDAO.find(name);
    }

    public int count() {
        return userDAO.count();
    }

    public Long nextTweetID() {
        return userDAO.nextTweetID();
    }

    public void addRole(String rolename) {
        Role role = new Role(rolename);
        securityDAO.addRole(role);
    }

    public void addUserRole(String username, String rolename) {
        User user = userDAO.find(username);
        Role role = securityDAO.getRole(rolename);
        securityDAO.addUserRole(user, role);
    }

}
