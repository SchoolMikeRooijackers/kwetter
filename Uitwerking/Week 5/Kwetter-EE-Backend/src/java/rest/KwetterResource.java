/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest;

import domain.Tweet;
import domain.User;
import java.util.List;
import java.util.Properties;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import service.KwetterService;

/**
 * REST Web Service
 *
 * 
 */
@Path("")
@RequestScoped
public class KwetterResource {

    @Context
    private UriInfo context;

    @Inject
    KwetterService kwetterService;

    /**
     * Creates a new instance of KwetterResource
     */
    public KwetterResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("api")
    public List<User> findAllUsers() {
        return kwetterService.findAll();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("api/{userID}")
    public User findUser(@PathParam("userID") Long id) {
        return kwetterService.find(id);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("api/count")
    public int count() {
        return kwetterService.count();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("api/{userID}")
    public String addTweet(Tweet tweet, @PathParam("userID") Long userID) {
        Boolean succes;
        String message = "";

        User user = kwetterService.find(userID);
        //We need to use new Tweet() here so the id can be given in the contructor 
        succes = user.addTweet(new Tweet(tweet.getTweetText(), tweet.getDatum(), tweet.getVanaf()));
        if (!succes) {
            message = "Error adding tweet";
        }
        kwetterService.edit(user);

        return String.format("{\"succes\":\"%b\",\"message\":\"%s\"}", succes, message);
    }

    @GET
    @Path("api/startBatch")
    @Produces(MediaType.TEXT_PLAIN)
    public String startBatch() {
        JobOperator jo = BatchRuntime.getJobOperator();
        //  jo.getJobInstances(null, start, count)
        long jid = jo.start("kwetterJob", new Properties());
        return "Batch submitted: " + jid;
    }
    
    @POST
    @Path("roles/add")
    @Consumes("application/json")    
    public void addRole(@Context UriInfo ui){ 
        MultivaluedMap<String, String> map = ui.getQueryParameters();
        String rolename = map.get("role").get(0);
        kwetterService.addRole(rolename);
    }
    
    @POST
    @Path("roles/addToUser/{username}")
    @Consumes("application/json")    
    public void addRoleToUser(@Context UriInfo ui, @PathParam("username")String username){ 
        MultivaluedMap<String, String> map = ui.getQueryParameters();
        String rolename = map.get("role").get(0);
        kwetterService.addUserRole(username, rolename);
    }
}
