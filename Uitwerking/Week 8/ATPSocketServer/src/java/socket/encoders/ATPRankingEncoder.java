
package socket.encoders;


import java.io.StringWriter;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import socket.messages.ATPRankingMessage;
import socket.messages.Player;

public class ATPRankingEncoder implements Encoder.Text<ATPRankingMessage> {
    
    @Override
    public void init(EndpointConfig ec) { }
    
    @Override
    public void destroy() { }
    
    @Override
    public String encode(ATPRankingMessage rankingMessage) throws EncodeException {
        StringWriter swriter = new StringWriter();
        try (JsonGenerator jsonGen = Json.createGenerator(swriter)) {
            jsonGen.writeStartArray();
            rankingMessage.getPlayers().stream().forEach((player) -> {
                writeJSONPlayer(jsonGen, player);
            });          
            jsonGen.writeEnd();
        }
        return swriter.toString();
    }

    private void writeJSONPlayer(JsonGenerator jsonGen, Player player) {
        jsonGen.writeStartObject()
                .write("name", player.getName())
                .write("email", player.getEmail())
                .write("rank", player.getRank())
                .writeEnd();
    }
}
