package socket.messages;

public class Player {

    private String name;
    private String email;
    private String rank;

    public Player(String name, String email, String rank) {
        this.name = name;
        this.email = email;
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

}
