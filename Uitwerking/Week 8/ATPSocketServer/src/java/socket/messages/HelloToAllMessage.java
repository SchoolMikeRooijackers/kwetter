package socket.messages;

public class HelloToAllMessage extends Message {

    private String message;

    public HelloToAllMessage(String message) {
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    /* For logging purposes */
    @Override
    public String toString() {
        return "[HelloToAllMessage] " + "-" + message;
    }
}
