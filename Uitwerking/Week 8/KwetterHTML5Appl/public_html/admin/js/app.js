angular.module('Kwetter', ['ngWebSocket'])
        .factory('Kwetter', function ($websocket) {
            // Open a WebSocket connection
            var url = "ws://localhost:8080/Kwetter-EE-Backend/kwetterendpoint";
            var ws = $websocket(url);

            var Kwetter = [];

            ws.onMessage(function (event) {
                console.log('message: ', event.data);
                var response;
                try {
                    response = angular.fromJson(event.data);
                } catch (e) {
                    document.getElementById("helloId").innerHTML =
                            "Sorry, connection failed ...";
                    document.getElementById("btnAtpId").disabled = false;
                    console.log('error: ', e);
                    response = {'error': e};
                }

                if (response.hello) {
                    document.getElementById("helloId").innerHTML = response.hello;
                    document.getElementById("btnAtpId").disabled = false;
                } else if (response.helloAll) {
                    document.getElementById("helloAllId").innerHTML = response.helloAll;
                } else {
                    for (var i = 0; i < response.length; i++) {
                        Kwetter.push({
                            rank: response[i].rank,
                            name: response[i].name,
                            email: response[i].email
                        });
                    }
                }
            });
            ws.onError(function (event) {
                console.log('connection Error', event, null);
            });
            ws.onClose(function (event) {
                console.log('connection closed', event, null);
            });
            ws.onOpen(function () {
                console.log('connection open');
                ws.send('HELLO SERVER');
            });

            return {
                Kwetter: Kwetter,
                status: function () {
                    return ws.readyState;
                },
                send: function (message) {
                    if (angular.isString(message)) {
                        ws.send(message);
                    }
                    else if (angular.isObject(message)) {
                        ws.send(JSON.stringify(message));
                    }
                }
            };
        })
        .controller('kwetterController', function ($scope, Kwetter) {
            $scope.Kwetter = Kwetter;

            $scope.submit = function () {
                Kwetter.send("ATP SERVER");
            };

            $scope.submitAll = function () {
                Kwetter.send("HELLO TO ALL");
            };
        });