package socket.messages;

import java.util.ArrayList;
import java.util.List;

public class ATPRankingMessage extends Message {

    private final List<Player> players = new ArrayList<>();

    public ATPRankingMessage() {

    }

    public List<Player> getPlayers() {
        return players;
    }

    public void addPlayer(Player p) {
        players.add(p);
    }

    /* For logging purposes */
    @Override
    public String toString() {
        return "[HelloToAllMessage] " + "-" + players;
    }
}
