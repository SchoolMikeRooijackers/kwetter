package socket;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import socket.encoders.ATPRankingEncoder;
import socket.encoders.HelloServerMessageEncoder;
import socket.encoders.HelloToAllMessageEncoder;
import socket.messages.ATPRankingMessage;
import socket.messages.HelloServerMessage;
import socket.messages.*;

@ServerEndpoint(
        value = "/kwetterendpoint",
        encoders = {HelloServerMessageEncoder.class, HelloToAllMessageEncoder.class, ATPRankingEncoder.class}
)
public class KwetterWebsocketEndpoint {

    private static final Logger LOG = Logger.getLogger(KwetterWebsocketEndpoint.class.getName());
    private static final Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());

    @OnMessage
    public void onMessage(final Session client, final String message) {
        if (message != null) {
            switch (message) {
                case "HELLO SERVER":
                    sendMessage(client, new HelloServerMessage("Hello, if you need the Kwetter list just press the button ..."));
                    break;
                case "HELLO TO ALL":
                    java.util.Date date = new java.util.Date();
                    sent2All(new HelloToAllMessage("HELLO TO ALL AT " + date.toString() + " ..."));
                    break;
                case "Kwetter SERVER":
                    ATPRankingMessage ranking = initRanking();
                    sendMessage(client, ranking);
                    break;
            }
        }

    }

    private ATPRankingMessage initRanking() {
        ATPRankingMessage ranking = new ATPRankingMessage();
        ranking.addPlayer(new Player("Nadal, Rafael (ESP)", "nadalrafael@gmail.com", "1"));
        ranking.addPlayer(new Player("Djokovic, Novak (SRB)", "djokovicnovak@gmail.com", "2"));
        ranking.addPlayer(new Player("Federer, Roger (SUI)", "federerroger@gmail.com", "3"));
        ranking.addPlayer(new Player("Wawrinka, Stan (SUI)", "wawrinkastan@gmail.com", "4"));
        ranking.addPlayer(new Player("Ferrer, David (ESP)", "ferrerdavid@gmail.com", "5"));
        return ranking;
    }

    private void sendMessage(Session peer, Object send) {

        try {
            if (peer.isOpen()) {
                peer.getBasicRemote().sendObject(send);
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    @OnOpen
    public void onOpen(Session peer) {
        LOG.info("Connection opened ...");
        peers.add(peer);
    }

    @OnClose
    public void onClose(Session peer) {
        LOG.info("Connection closed ...");
        peers.remove(peer);
    }

    @OnError
    public void onError(Throwable t) {
        LOG.log(Level.INFO, "Foutje ...{0}", t.getMessage());
    }

    private void sent2All(Object answer) {

        peers.stream().forEach((peer) -> {
            sendMessage(peer, answer);
        });
    }
}
