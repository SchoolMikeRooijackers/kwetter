package socket.encoders;

import java.io.StringWriter;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import socket.messages.HelloToAllMessage;

public class HelloToAllMessageEncoder implements Encoder.Text<HelloToAllMessage> {

    @Override
    public void init(EndpointConfig ec) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public String encode(HelloToAllMessage helloToAllMessage) throws EncodeException {
        StringWriter swriter = new StringWriter();
        try (JsonGenerator jsonGen = Json.createGenerator(swriter)) {
            jsonGen.writeStartObject()
                    .write("helloAll", helloToAllMessage.getMessage())
                    .writeEnd();
        }
        return swriter.toString();
    }
}
