/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest;

import domain.Tweet;
import domain.User;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import service.KwetterService;

/**
 * REST Web Service
 *
 */
@Path("")
@Stateless
public class KwetterResource {

    @Inject
    KwetterService kwetterService;

    /**
     * Creates a new instance of KwetterResource
     */
    public KwetterResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("api")
    public List<User> findAllUsers() {
        return kwetterService.findAll();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("api/{userID}")
    public User findUser(@PathParam("userID") Long id) {
        return kwetterService.find(id);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("api/count")
    public int count() {
        return kwetterService.count();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("api/{userID}")
    @RolesAllowed("user_role")
    public String addTweet(Tweet tweet, @PathParam("userID") Long userID) {
        //TODO: USER ID CAN BE REPLACED BY request.getRemoteUser() FOR THE USERNAME OF LOGGED IN USER
        Boolean succes;
        String message = "";

        User user = kwetterService.find(userID);
        //We need to use new Tweet() here so the id can be given in the contructor 
        succes = user.addTweet(new Tweet(tweet.getTweetText(), tweet.getDatum(), tweet.getVanaf()));
        if (!succes) {
            message = "Error adding tweet";
        }
        kwetterService.edit(user);

        return String.format("{\"succes\":\"%b\",\"message\":\"%s\"}", succes, message);
    }

    @GET
    @Path("api/startBatch")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("admin_role")
    public String startBatch() {
        JobOperator jo = BatchRuntime.getJobOperator();
        long jid = jo.start("kwetterJob", new Properties());
        return "Job submitted: " + jid;
    }

    @POST
    @Path("api/login")
    @PermitAll
    public Response login(
            @FormParam("username") String username,
            @FormParam("password") String password,
            @Context HttpServletRequest request) {
        try {
            request.getSession();
            request.login(username, password);
        } catch (Exception ex) {
            Logger.getLogger(KwetterResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().entity(ex.getLocalizedMessage()).build();
        }
        return Response.ok("loggedin.html?id=" + kwetterService.find(username).getId()).build();
    }

    @GET
    @Path("api/logout")
    @RolesAllowed({"user_role", "admin_role"})
    public Response logout(@Context HttpServletRequest request) {
        try {
            request.logout();
            request.getSession().invalidate();
        } catch (ServletException ex) {
            Logger.getLogger(KwetterResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().entity(ex.getLocalizedMessage()).build();
        }
        return Response.ok("logged out").build();
    }

    @GET
    @Path("api/isLoggedIn/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response isLoggedIn(@PathParam("id") Long id, @Context HttpServletRequest request) {
        String username = request.getRemoteUser();
        if (username == null) {
            return Response.serverError().entity("{\"message\":\"No user is logged in\"}").build();
        }
        User user = kwetterService.find(username);
        if (Objects.equals(user.getId(), id)) {
            return Response.ok("{\"message\":\"Ok\"}").build();
        } else {
            return Response.serverError().entity("{\"message\":\"Other user is logged in\",\"id\":\"" + user.getId() + "\"}").build();
        }
    }

    @GET
    @Path("api/currentRole")
    @PermitAll
    public String getCurrentRole(@Context HttpServletRequest request) {
        request.getSession();
        String[] allRoles = {"user_role", "admin_role"};
        List userRoles = new ArrayList(allRoles.length);
        for (String role : allRoles) {
            if (request.isUserInRole(role)) {
                userRoles.add(role);
            }
        }
        return Arrays.toString(userRoles.toArray());
    }

    @GET
    @Path("api/isAdmin")
    @RolesAllowed("admin_role")
    public String isAdmin() {
        return "Current user is logged in as admin";
    }

    @GET
    @Path("api/isUser")
    @RolesAllowed("user_role")
    public String isUser() {
        return "Current user is logged in as user";
    }

    @DELETE
    @Path("api/{id}")
    @RolesAllowed("admin_role")
    public String delTweet(@PathParam("id") Long id) {
        Tweet tweet = null;
        for(User u : kwetterService.findAll()){
            for(Tweet t : u.getTweets()){
                if(t.getId() == id){
                    tweet = t;
                }
            }
        }
        if(tweet == null){
            return "Couldn't find tweet";
        }
        kwetterService.remove(tweet);
        return "OK";
    }
}
