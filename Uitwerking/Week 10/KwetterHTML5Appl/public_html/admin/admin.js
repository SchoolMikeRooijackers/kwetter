;
(function () {
    function isAdmin() {
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/Kwetter-EE-Backend/rest/api/isAdmin",
            xhrFields: {
                withCredentials: true
            }
        }).success(function (data) {
            console.log("is admin: success");
            console.log(data)
        }).error(function (data) {
            console.log("is admin: error");
            console.log(data)
        });
    }
    
    isAdmin();
    
    var app = angular.module('Kwetter', ['ngResource']);
    app.factory("adminFactory", ['$resource', '$http', function ($resource, $http) {
            $http.defaults.withCredentials = true;
            return $resource("http://localhost:8080/Kwetter-EE-Backend/rest/api/:id");
        }]);
    app.controller("Kwetter_admin", ['$scope', 'adminFactory', function ($scope, adminFactory) {
            $scope.users = [];
            $scope.loadTweets = function () {
                adminFactory.query(function (data) {
                    console.log("load tweets:");
                    console.log(data);

                    $scope.users = data;
                });
            };
            $scope.loadTweets();

            $scope.getTweets = function () {
                var timeline = [];
                for (i in $scope.users) {
                    for (j in $scope.users[i].tweets) {
                        var tweet = $scope.users[i].tweets[j];
                        tweet.user = $scope.users[i].name;
                        timeline.push(tweet);
                    }
                }
                timeline.sort(function (a, b) {
                    return a.id == b.id ? 0 : a.id < b.id ? 1 : -1;
                });
                return timeline;
            };

            $scope.delTweet = function () {
                console.log(this);
                adminFactory.delete({id: this.tweet.id}).$promise.finally(function () {
                    $scope.loadTweets();
                });
            };
        }]);
}());