/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kwetterbatch;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;
import java.sql.Connection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Daan
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try (Connection connection = DBConnection.getConnection()) {
            connection.setAutoCommit(false);

            try (PreparedStatement pstmt = connection.prepareStatement("Insert into table1 (id,tweet, nummer) values (?,?,?)");) {

                JsonReader reader = new JsonReader();

                Object jsonObject = reader.read();
               
                System.out.println(jsonObject.toString());
                System.out.println("------------");
                
                JSONObject object = (JSONObject)jsonObject;
                
                System.out.println(object.toString());
                System.out.println("-------------");
                
                while (object.keySet().iterator().hasNext()) {
                    Object obj = (Object) jsonObject;
                    
                    JSONArray array = (JSONArray) obj;
                    
                    System.out.println(array.toJSONString());

                    String name = (String) array.get(0);
                    String tweet = (String) array.get(1);
                    String from = (String) array.get(2);
                    String date = (String) array.get(3);

                }

                pstmt.setString(1, "123.45");
                pstmt.setLong(2, 2345678912365L);
                pstmt.setLong(3, 1234567L);
                pstmt.addBatch();

                pstmt.setString(1, "456.00");
                pstmt.setLong(2, 567512198454L);
                pstmt.setLong(3, 1245455L);
                pstmt.addBatch();

                pstmt.setString(1, "7859.02");
                pstmt.setLong(2, 659856423145L);
                pstmt.setLong(3, 5464845L);
                pstmt.addBatch();

                int[] arr = pstmt.executeBatch();
                System.out.println(Arrays.toString(arr));

                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                connection.rollback();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
