/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kwetterbatch;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Daan
 */
public class JsonReader {

    JsonReader() {
    }

  
    public Object read() throws org.json.simple.parser.ParseException, IOException {
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(new FileReader("D:\\NetBeansProjects\\kwetterBatch\\input.json"));
        System.out.println(obj.toString());
        return obj;
    }

}
