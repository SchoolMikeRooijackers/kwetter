/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kwetterbatch;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Daan
 */
public class DBConnection {

    public static Connection getConnection() throws SQLException, ClassNotFoundException {

        Class.forName("com.mysql.jdbc.Driver");

        // connect way #1
        String url1 = "jdbc:mysql://localhost:3306/kwetterbatch";
        String user = "root";
        String password = "root";

        Connection connection = DriverManager.getConnection(url1, user, password);
        if (connection != null) {
            System.out.println("Connected to the database test1");
        }

        return connection;

    }

}
